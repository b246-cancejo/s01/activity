name = "Windel"
age = 18
occupation = "Software Developer"
movie = "Black Panther" 
rating = 99.6

print(f"I am {name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating} %")

num1, num2, num3 = 1, 150, 6
print(num1 * num2)
print(num1 < num3)
print(num3 + num2)